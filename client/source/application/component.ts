import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'root',
  templateUrl: './../../../theme/partials/index.pug',
  encapsulation: ViewEncapsulation.None
})
export class RootComponent {

  constructor() {}

}
