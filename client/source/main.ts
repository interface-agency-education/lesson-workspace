import './polyfills.browser';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { bootloader } from '@angularclass/hmr';
import { RootModule } from './application/module';
import { decorateModuleRef } from './environment';

export function main() {
	return platformBrowserDynamic().bootstrapModule(RootModule);
}

bootloader(main);
