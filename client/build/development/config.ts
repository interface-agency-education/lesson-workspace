const { EXCLUDE_SOURCE_MAPS } = require('./../shared/constants');

const { ContextReplacementPlugin, DllReferencePlugin, HotModuleReplacementPlugin, NoEmitOnErrorsPlugin } = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const { root } = require('./../shared/helpers.js');
const ip = require('ip');

module.exports = {
    mode: 'development',
	target: 'web',
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'source-map-loader',
            exclude: [EXCLUDE_SOURCE_MAPS]
        }, {
            test: /\.ts$/,
            loaders: [
                '@angularclass/hmr-loader', {
                    loader: 'awesome-typescript-loader',
                    options: {
                        configFileName: root('tsconfig.json')
                    }
                },
                'angular2-template-loader',
                'angular-router-loader'
            ],
            exclude: [/\.(spec|e2e|d)\.ts$/]
		}, {
	        test: /\.(pug)$/,
	        loaders: ['raw-loader', {
				loader: 'pug-html-loader',
				options: {
					doctype: 'html'
				}
			}],
			include: [root('../theme/partials')]
		}, {
            test: /\.styl$/,
            loader: 'style-loader!css-loader!stylus-loader',
            include: root('../theme/styles')
        }]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json', '.pug', 'styl']
    },
	profile: true,
    cache: true,
	// stats: {
	//     assetsSort: '!size'
	// },
	// context: root(''),
    entry: {
		main: root('source/main.ts')
	},
    output: {
        path: root('dist'),
		filename: 'index.js',
        // publicPath: "/"
    },
	performance: {
		hints: false
		// hints: 'warning'
	},
	watch: true,
	devServer: {
	  contentBase: root('dist'),
	  port: 8081,
	  hot: true,
	  clientLogLevel: 'silent',
	  historyApiFallback: {
		disableDotRule: true,
	  },
	  // open: 'Firefox',
	  stats: 'minimal',
	  // host: ip.address()
  },
    plugins: [
        new CopyPlugin([{
            from: root('dll')
        }, {
            from: root('../theme/assets')
        }]),
        new FilterWarningsPlugin({ exclude: /System\.import/ }),
		new ContextReplacementPlugin(
			/\@angular(\\|\/)core(\\|\/)fesm5/,
			root('dist'), {}
		),
		new DllReferencePlugin({ context: '.', manifest: require('./../../dll/polyfill-manifest.json') }),
		new DllReferencePlugin({ context: '.', manifest: require('./../../dll/vendor-manifest.json') }),

        new HtmlWebpackPlugin({
            template: root('source/index.html')
        })
    ]
};
